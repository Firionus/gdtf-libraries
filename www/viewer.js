function render_fixture(data) {
    var container, controls;
    var camera, scene, raycaster, renderer;
    var mouse = new THREE.Vector2(),
        INTERSECTED;
    var radius = 100,
        theta = 0;
    init();
    animate();

    function init() {
        //container = document.createElement( 'div' );
        container = document.getElementById("render")
        //document.body.appendChild( container );
        camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight,
            0.1, 2000);
        camera.position.y = -220;
        camera.position.x = 00;
        camera.position.z = 100;
        controls = new THREE.TrackballControls(camera, container);
        scene = new THREE.Scene();
        //scene.add( new THREE.HemisphereLight() );
        scene.background = new THREE.Color(0xfffffff);
        var directionalLight = new THREE.DirectionalLight(0xffeedd);
        directionalLight.position.set(0, 0, 2);
        scene.add(directionalLight);
        var directionalLight2 = new THREE.DirectionalLight(0xffeedd);
        directionalLight2.position.set(2, 2, 2);
        scene.add(directionalLight2);
        var ambientLight = new THREE.AmbientLight(0xcccccc, 0.8);
        scene.add(ambientLight);

	var gridXZ = new THREE.GridHelper(400, 10, 0x0,0xffff00);
	gridXZ.position.set( -100,100,-100 );
	scene.add(gridXZ);
	
	var gridXY = new THREE.GridHelper(400, 10,0x0,0x00ffff);
	gridXY.position.set( -100,-100,100 );
	gridXY.rotation.x = Math.PI/2;
	scene.add(gridXY);
	var gridYZ = new THREE.GridHelper(400, 10,0x0,0xff00ff);
	gridYZ.position.set( 100,-100,-100 );
	gridYZ.rotation.z = Math.PI/2;
	scene.add(gridYZ);


        var pointLight = new THREE.PointLight(0xffffff, 0.8);
        //camera.add( pointLight );
        scene.add(camera);
        //3ds files dont store normal maps
        var manager = new THREE.LoadingManager();
        manager.onProgress = function(item, loaded, total) {
            console.log(item, loaded, total);
        };
        var onProgress = function(xhr) {
            if (xhr.lengthComputable) {
                var percentComplete = xhr.loaded / xhr.total * 100;
                console.log(Math.round(percentComplete, 2) +
                    '% downloaded');
            }
        };
        var onError = function(xhr) {};
        //var i_loader = new THREE.ImageLoader();
        //var texture = new THREE.Texture();
        //i_loader.setPath('./');
        //i_loader.load('wood-texture.jpg', function(image) {
        //texture.wrapT = THREE.RepeatWrapping
        //texture.image = image;
        //texture.needsUpdate = true;
        //});
        console.log(data["models"]);
        data["models"].forEach(function(model) {
            console.log(model);

            function getRandomColor() {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }
            document.objects = []
            if (model.file == "beam") {
                geometry = new THREE.CylinderGeometry(model.width *
                    100, model.width * 100, 1, 16);
                material = new THREE.MeshBasicMaterial({
                    color: 'gold',
                    wireframe: false
                });
                beam = new THREE.Mesh(geometry, material);
                beam.position.z = model.offset * 200;
                console.log(model.offset)
                beam.rotation.x = 1.6
                //beam.scale.set(0.2,0.2,0.2);
                scene.add(beam);
                document.objects.push(model.name)
                document.objects[model.name] = beam
            } else {
                var loader = new THREE.TDSLoader(manager);
                loader.load(model.file + '.3ds', function(object) {
                    switch (model.name) {
                        case "Body":
                            color = "dimgrey";
                            break;
                        case "Base":
                            color = "dimgrey";
                            break;
                        case "Yoke":
                            color = "darkcyan";
                            break;
                        case "Head":
                            color = "darkslategrey";
                            break;
                        default:
                            color = getRandomColor()
                    }
                    material = new THREE.MeshPhongMaterial({
                        color: color,
                        wireframe: false
                    });
                    object.traverse(function(child) {
                        if (child instanceof THREE.Mesh) {
                            child.material =
                                material;
                        }
                    });
                    object.material = material
                    object.position.z = model.offset * 200;
                    object.scale.set(0.2, 0.2, 0.2);
                    scene.add(object);
                    document.objects.push(model.name)
                    document.objects[model.name] = object
                }, onProgress, onError);
            }
        });
        raycaster = new THREE.Raycaster();
        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth / 2.5, window.innerHeight / 2.5);
        //renderer.setSize( container.offsetWidth, container.offsetWidth/1.9 );
        //renderer.setSize( container.offsetWidth, container.offsetHeight );
        //renderer.setSize( 500,500 );
        //renderer.setClearColor( 0xffffff, 0);
        container.appendChild(renderer.domElement);
        window.addEventListener('resize', resize, false);
        document.addEventListener('mousemove', onDocumentMouseMove, false);
    }

    function onDocumentMouseMove(event) {
        event.preventDefault();
        //mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        //mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
        ////console.log("mouse move",mouse)
        //var rect = renderer.domElement.getBoundingClientRect();
        //mouse.x = ( ( event.clientX - rect.left ) / ( rect.width - rect.left ) ) * 2 - 1;
        //mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;
        //mouse.x = ( event.clientX / $(container).innerWidth ) * 2 - 1;
        //mouse.y = - ( event.clientY / $(container).innerHeight ) * 2 + 1;
        //var offset = $('.rightBlock').offset();
        //mouse.x = ( ( event.clientX - 0 ) / renderer.domElement.width ) * 2 - 1;
        //mouse.y = - ( ( event.clientY - 0 ) / renderer.domElement.height ) * 2 + 1;
    }

    function resize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    function animate() {
        controls.update();
        //renderer.render( scene, camera );
        requestAnimationFrame(animate);
        render();
    }

    function render() {
        //https://jsfiddle.net/prisoner849/9g1ecgw3/
        //theta += 0.1;
        //camera.position.x = radius * Math.sin( THREE.Math.degToRad( theta ) );
        //camera.position.y = radius * Math.sin( THREE.Math.degToRad( theta ) );
        //camera.position.z = radius * Math.cos( THREE.Math.degToRad( theta ) );
        //camera.lookAt( scene.position );
        //camera.updateMatrixWorld(); 
        //raycaster.setFromCamera( mouse, camera );
        //var intersects = raycaster.intersectObjects( scene.children );
        ////if (INTERSECTED){
        //if ( intersects.length > 0 ) {
        //    console.log(intersects)
        //	if ( INTERSECTED != intersects[ 0 ].object ) {
        //		if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
        //		INTERSECTED = intersects[ 0 ].object;
        //		INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
        //		INTERSECTED.material.emissive.setHex( 0xff0000 );
        //	}
        //} else {
        //	if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
        //	INTERSECTED = null;
        //}
        //}
        renderer.render(scene, camera);
    }
};
