[![pipeline status](https://gitlab.com/petrvanek/gdtf-libraries/badges/master/pipeline.svg)](https://gitlab.com/petrvanek/gdtf-libraries/commits/master)


ROBE GDTF files.
XML Schema for validation.
Simple loader: https://petrvanek.gitlab.io/gdtf-libraries/
.
