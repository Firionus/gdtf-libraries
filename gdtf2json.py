from lxml import etree
import json
import re

# import os
from glob import glob

from colorspacious import cspace_convert

# os.chdir(os.path.dirname(os.path.abspath(__file__)))

def clamp(x): 
  return max(0, min(x, 255))

def cie2rgbhex(color):
    if color is not None:
        xyY = [float(x) for x in color.split(",")]
        xyY[2]=xyY[2]/100 # compensate for temporary Y*100 due to lack of definition of Y range
        r, g, b = [
            clamp(int(x))
            for x in cspace_convert(xyY, {"name": "xyY100"}, {"name": "sRGB255"})
        ]
        return "#{0:02x}{1:02x}{2:02x}".format(r, g, b)
    return None


def convert(file_name, _dir):
    data = {}

    robe_fixture = etree.parse(file_name, etree.XMLParser(recover=True)).getroot()
    #ns = {"ns": "https://gdtf-share.com/Device"}
    ns = {"ns": "http://schemas.gdtf-share.com/device"}

    data["name"] = robe_fixture.xpath("//ns:GDTF/ns:FixtureType", namespaces=ns)[0].get(
        "Name"
    )
    # print(data["name"])
    data["description"] = robe_fixture.xpath("//ns:GDTF/ns:FixtureType", namespaces=ns)[
        0
    ].get("Description")
    data["image"] = "{}{}".format(
        _dir,
        robe_fixture.xpath("//ns:GDTF/ns:FixtureType", namespaces=ns)[0].get(
            "Thumbnail"
        ),
    )

    data["models"] = []

    def get_models(cosi, offset=0):
        if cosi is not None:
            for i in cosi:
                name = i.get("Name")
                model_offset = i.get("Position", 0)
                if model_offset:
                    model_offset = re.findall("\{(.*?)\}", model_offset)
                    # print(model_offset)
                    if model_offset:
                        model_offset = float(model_offset[2].split(",")[3])
                if robe_fixture.xpath(
                    '//ns:GDTF/ns:FixtureType/ns:Models/ns:Model[@Name="{}"]'.format(
                        name
                    ),
                    namespaces=ns,
                ):
                    model_file_name = robe_fixture.xpath(
                        '//ns:GDTF/ns:FixtureType/ns:Models/ns:Model[@Name="{}"]'.format(
                            name
                        ),
                        namespaces=ns,
                    )[0].get("File", False)
                    if model_file_name:
                        data["models"].append(
                            {
                                "file": "{}models/3ds/{}".format(_dir, model_file_name),
                                "offset": model_offset + offset,
                                "name": name,
                            }
                        )
                    else:
                        if name == "Beam":
                            model_width = robe_fixture.xpath(
                                '//ns:GDTF/ns:FixtureType/ns:Models/ns:Model[@Name="{}"]'.format(
                                    name
                                ),
                                namespaces=ns,
                            )[0].get("Width", False)
                            data["models"].append(
                                {
                                    "file": "beam",
                                    "offset": model_offset + offset,
                                    "width": model_width,
                                    "name": name,
                                }
                            )

                get_models(i, model_offset + offset)

    root = robe_fixture.xpath(
        "//ns:GDTF/ns:FixtureType/ns:Geometries/ns:*[@Model]", namespaces=ns
    )
    get_models(root)

    data["dmxmodes"] = []
    for mode in robe_fixture.xpath(
        "//ns:GDTF/ns:FixtureType/ns:DMXModes/ns:DMXMode", namespaces=ns
    ):
        name = mode.get("Name", "No name")

        def cC(minus=-1):
            countC = mode.xpath("ns:DMXChannels/ns:DMXChannel[@Offset]", namespaces=ns)[
                minus
            ].get("Offset", 0)
            if countC == "None":
                return cC(minus - 1)
            else:
                return countC

        countC = cC()
        count=countC.split(',')[-1]


        data["dmxmodes"].append({"name": name, "count": count})

    data["revisions"] = []
    for revision in robe_fixture.xpath(
        "//ns:GDTF/ns:FixtureType/ns:Revisions/ns:Revision", namespaces=ns
    ):
        data["revisions"].append(
            {"date": revision.get("Date"), "text": revision.get("Text")}
        )

    data["wheels"] = []
    for wheel in robe_fixture.xpath(
        "//ns:GDTF/ns:FixtureType/ns:Wheels/ns:Wheel", namespaces=ns
    ):
        name = wheel.get("Name", "No name")
        slots = []

        for slot in wheel.xpath("ns:Slot", namespaces=ns):
            slots.append(
                {
                    "name": slot.get("Name"),
                    "image": "{}wheels/{}".format(_dir, slot.get("MediaFileName", None)),
                    "color": cie2rgbhex(slot.get("Color", None)),
                }
            )

        data["wheels"].append({"name": name, "slots": slots})

    data["emitters"] = []
    data["spd"] = "No"

    if len(robe_fixture.xpath(
        "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Filters/ns:Filter",
        namespaces=ns,
    )):

        for emitter in robe_fixture.xpath(
            "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Filters/ns:Filter",
            namespaces=ns,
        ):
            name = emitter.get("Name", "No name")
            data["emitters"].append(
                {"name": name, "color": cie2rgbhex(emitter.get("Color", None))}
            )

        if len(
            robe_fixture.xpath(
                "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Filters//ns:Measurement",
                namespaces=ns,
            )
        ):
            data["spd"] = {}
            data["spd"]["colors"] = []

            for i in robe_fixture.xpath(
                "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Filters/ns:Filter",
                namespaces=ns,
            ):
                color_values = []
                for j in robe_fixture.xpath(
                        '//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Filters/ns:Filter[@Name="{}"]/ns:Measurement/ns:MeasurementPoint'.format(
                        i.get("Name")
                    ),
                    namespaces=ns,
                ):
                    color_values.append(j.get("Energy"))

                data["spd"]["colors"].append(
                    {
                        "name": i.get("Name"),
                        "data": color_values,
                        "className": i.get("Name"),
                    }
                )

                if not data["spd"].get("labels", False):
                    data["spd"]["labels"] = []
                    for j in robe_fixture.xpath(
                        '//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Filters/ns:Filter[@Name="{}"]/ns:Measurement'.format(
                            i.get("Name")
                        ),
                        namespaces=ns,
                    ):
                        data["spd"]["labels"].append(j.get("WaveLength"))



    if len(robe_fixture.xpath(
        "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Emitters/ns:Emitter",
        namespaces=ns,
    )):

        for emitter in robe_fixture.xpath(
            "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Emitters/ns:Emitter",
            namespaces=ns,
        ):
            name = emitter.get("Name", "No name")
            data["emitters"].append(
                {"name": name, "color": cie2rgbhex(emitter.get("Color", None))}
            )

        if len(
            robe_fixture.xpath(
                "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Emitters//ns:Measurement",
                namespaces=ns,
            )
        ):
            data["spd"] = {}
            data["spd"]["colors"] = []

            for i in robe_fixture.xpath(
                "//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Emitters/ns:Emitter",
                namespaces=ns,
            ):
                color_values = []
                for j in robe_fixture.xpath(
                        '//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Emitters/ns:Emitter[@Name="{}"]/ns:Measurement/ns:MeasurementPoint'.format(
                        i.get("Name")
                    ),
                    namespaces=ns,
                ):
                    color_values.append(j.get("Energy"))

                data["spd"]["colors"].append(
                    {
                        "name": i.get("Name"),
                        "data": color_values,
                        "className": i.get("Name"),
                    }
                )

                if not data["spd"].get("labels", False):
                    data["spd"]["labels"] = []
                    for j in robe_fixture.xpath(
                        '//ns:GDTF/ns:FixtureType/ns:PhysicalDescriptions/ns:Emitters/ns:Emitter[@Name="{}"]/ns:Measurement'.format(
                            i.get("Name")
                        ),
                        namespaces=ns,
                    ):
                        data["spd"]["labels"].append(j.get("WaveLength"))


    data["beam"] = []
    for x, v in robe_fixture.xpath(
        "//ns:GDTF/ns:FixtureType/ns:Geometries//ns:Beam", namespaces=ns
    )[0].items():
        if x not in ["Position", "Model", "Name"]:
            data["beam"].append({"name": x, "value": v})

    return data


fixtures = {}

directories = glob("./*/")

for _dir in directories:
    details = convert("{}description.xml".format(_dir), _dir)
    fixtures[_dir[2:-1]] = {"details": details}

#print(json.dumps(fixtures, indent=4, sort_keys=True))
print(json.dumps(fixtures))
